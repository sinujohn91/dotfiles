local gauth = require "gauth"
require "token"

local VPNToggle = {}

VPNToggle.VPNs = {}

VPNToggle.chooser = {}

local get_all_vpns_script = [[
    tell application "System Events"
      tell process "SystemUIServer"
        set vpnMenu to (menu bar item 1 of menu bar 1 where description is "VPN")
        tell vpnMenu to click
        set vpnMenuItems to (menu items of menu 1 of vpnMenu)
        -- Loop till first missing value(that is fisrt menu item seperator) and accumulate VPN Names
        set vpnNames to {}
        repeat with vpnMenuItem in vpnMenuItems
          set vpnName to name of vpnMenuItem
          if vpnName is equal to missing value then
            exit repeat
          end if
          set vpnNames to vpnNames & {vpnName}
        end repeat
        key code 53
        get vpnNames
      end tell
    end tell
]]

local connect_to_selected_vpn_script = [[
    on connectVpn(vpnName, password)
    tell application "System Events"
      tell process "SystemUIServer"
        set vpnMenu to (menu bar item 1 of menu bar 1 where description is "VPN")
        tell vpnMenu to click
        try
          click menu item vpnName of menu 1 of vpnMenu
          if not (vpnName contains "Disconnect") then
            delay 2
            keystroke password
            keystroke return
          end if
        on error errorStr
          key code 53
          if errorStr does not contain "Can’t get menu item" and errorStr does not contain vpnName then
            display dialog errorStr
          end if
        end try
      end tell
    end tell
  end connectVpn

  connectVpn("$vpnName", "$password")
]]

function VPNToggle.get_all_vpns()
    local success,vpns = hs.osascript.applescript(get_all_vpns_script)
    if success then
        for key, value in pairs(vpns) do
            VPNToggle.VPNs[key] = {text=value}
        end
    end
    return VPNToggle.VPNs
end

function VPNToggle.connect_to_selected_vpn(vpn_object)
    if vpn_object == nil then return end
    local vpn_to_connect = vpn_object["text"]
    local token = password_from_keychain("token_gojek_gate")
    local hash = gauth.GenCode(token, math.floor(os.time() / 30))
    hash = ("%06d"):format(hash)

    local source = connect_to_selected_vpn_script:gsub("%$vpnName", vpn_to_connect):gsub("$password", hash)
    local success = hs.osascript.applescript(source)
    if success then
        hs.notify.show("VPN connect succeeded", "", "")
    else
        hs.notify.show("VPN connect failed", "", "")
    end
end

function VPNToggle.toggle_vpn_chooser_window()
    VPNToggle.chooser = hs.chooser.new(VPNToggle.connect_to_selected_vpn)
    VPNToggle.chooser:choices(VPNToggle.get_all_vpns)
    VPNToggle.chooser:show()
end

function VPNToggle.toggle_vpn_board()
    VPNToggle.toggle_vpn_chooser_window()
end

return VPNToggle
