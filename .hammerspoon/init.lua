hs.loadSpoon("ShiftIt")
spoon.ShiftIt:bindHotkeys({})

-- VPN toggler
local vpn_toggler = require "vpn-toggle"

hs.hotkey.bind({ "cmd", "alt" }, "c", function() -- set any binding you want
    vpn_toggler.toggle_vpn_board()
end)
