#! /bin/bash -e

print_command () {
    echo "===================================================================="
    echo "$1"
    echo "===================================================================="
}

set_raspi_config () {
    # TODO: make raspi-config also a single command
    print_command "Set Localisation,time zone,GPU memory split (cut it down to 8MB)"
    echo "Dont restart pi after setting the correct localisation and timezone"
    #sed --in-place ".bak" s/gpu_mem.*/gpu_mem=8/g /boot/config.txt
    raspi-config
}

add_utilities () {
    print_command "Adding basic utitlities"
    sudo apt-get update && sudo apt-get upgrade -y
    sudo apt install dnsutils
    sudo apt install git
    sudo apt install vim
    sudo apt install python3
    sudo apt install unattended-upgrades
    sudo apt install certbot
    sudo apt install jq
    sudo apt install curl
    print_command "Configure unattended-upgrades"
    sed 's/\/\/Unattended-Upgrade::Remove-Unused-Dependencies.*/Unattended-Upgrade::Remove-Unused-Dependencies "true";/' /etc/apt/apt.conf.d/50unattended-upgrades
    print_command "Setting up periodic upgrade"
    echo "APT::Periodic::Enable \"1\";" >> /etc/apt/apt.conf.d/02periodic
    echo "APT::Periodic::Update-Package-Lists \"1\";" >> /etc/apt/apt.conf.d/02periodic
    echo "APT::Periodic::Download-Upgradeable-Packages \"1\";" >> /etc/apt/apt.conf.d/02periodic 
    echo "APT::Periodic::Unattended-Upgrade \"1\";" >> /etc/apt/apt.conf.d/02periodic
    echo "APT::Periodic::AutocleanInterval \"1\";" >> /etc/apt/apt.conf.d/02periodic
    echo "APT::Periodic::Verbose \"2\";" >> /etc/apt/apt.conf.d/02periodic
    sudo unattended-upgrades -d
}

setup_static_ip() {
    print_command "Setup static IP"
    echo "Uncomment static IP configuration. Add DNS as 1.1.1.1"
    echo ""
    sudo vim /etc/dhcpcd.conf
    print_command "Save and reboot"
    print_command "Verify static IP and dns"
    ifconfig
    dig www.google.com
}

setup_ntp() {
    print_command "Configure NTP"
    timedatectl status
    echo "commend out last line and set ntp=time.cloudflare.com"
    echo "read more at https://blog.cloudflare.com/secure-time/"
    sudo vim /etc/systemd/timesyncd.conf
    sudo timedatectl set-ntp true
}

add_security_features() {
    print_command "Change default password for pi"
    passwd

    print_command "Securing your raspberry pi"
    print_command "Add new user"
    sudo adduser sinu
    sudo adduser sinu sudo
    sudo adduser sinu video

    print_command "Add a strong passwd for root"
    sudo passwd root

    print_command "Lock pi account"
    sudo passwd –-lock pi

    print_command "Setup ssh"
    sudo vi /etc/ssh/sshd_config
    echo "Add the following" /
    "# Authentication:
    LoginGraceTime 120
    PermitRootLogin no
    StrictModes yes

    RSAAuthentication yes
    PubkeyAuthentication yes
    AuthorizedKeysFile      %h/.ssh/authorized_keys

# To enable empty passwords, change to yes (NOT RECOMMENDED)
PermitEmptyPasswords no

PermitRootLogin prohibit-password
MaxAuthTries 6

# Change to yes to enable challenge-response passwords (beware issues with
# some PAM modules and threads)
ChallengeResponseAuthentication no

# Change to no to disable tunnelled clear text passwords
PasswordAuthentication no

UsePAM no"

print_command "Add ssh keys"

mkdir ~/.ssh
chmod 0700 ~/.ssh
touch ~/.ssh/authorized_keys
chmod 0600 ~/.ssh/authorized_keys

sudo service ssh restart

print_command "TODO: setup logwatch"

print_command "Install fail2ban"
sudo apt install fail2ban
echo "You can change configuration at /etc/fail2ban/jail.conf. And restart"

print_command "Install a firewall: ufw"
sudo apt install ufw
sudo ufw allow 80
sudo ufw allow 443
sudo ufw allow 53
sudo ufw allow 22
sudo ufw allow 8888
sudo ufw enable
sudo ufw status verbose

print_command "Performance"
print_command "Install Log2ram"
sudo echo "deb http://packages.azlux.fr/debian/ buster main" | sudo tee /etc/apt/sources.list.d/azlux.list
sudo wget -qO - https://azlux.fr/repo.gpg.key | sudo apt-key add -
sudo apt-get update
sudo apt install log2ram
sudo reboot
echo "Change Size=100M;LOG_DISK_SIZE=200M"
sudo vim /etc/log2ram.conf
sudo service log2ram restart
}

setup_monitoring () {
    print_command "Monitoring"
    print_command "Install RPi-Monitor"
    sudo apt-get install dirmngr
    sudo apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 2C0D3C0F
    sudo wget http://goo.gl/vewCLL -O /etc/apt/sources.list.d/rpimonitor.list
    sudo apt-get update
    sudo apt-get install rpimonitor
    echo "Uncomment the first two sections that start with “dynamic.10” and “dynamic.11”. Comment out the third, fourth and fifth lines in the next section that start with “web.status.1” and uncomment the last one. Uncomment the next section that starts with “web.statistics.1”. Exit and save."
    sudo vim /etc/rpimonitor/template/network.conf
    sudo service rpimonitor restart
    sudo /etc/init.d/rpimonitor update
}

setup_pihole() {
    print_command "Install pi-hole"
    sudo curl -sSL https://install.pi-hole.net | bash
    git clone https://github.com/TheSmashy/O365Whitlist.git  
    sudo python3 O365Whitlist/scripts/whitelist.py  
}

add_docker() {
    print_command "Adding docker"
    curl -sSL https://get.docker.com | sh
    sudo usermod -aG docker sinu
    sudo chmod 666 /var/run/docker.sock
    docker run hello-world
}

add_bitwarden() {
    print_command "Installing vaultwarden"
    docker pull vaultwarden/server:latest
    mkdir ~/bw-data
    read -s -p $'Enter admin key: \n'
    TOKEN=$REPLY
    read -s -p $'Enter SMPT API Key: \n'
    SMTP_API_KEY=$REPLY
    read -s -p $'Enter SMPT Secret: \n'
    SMTP_SECRET=$REPLY
    docker run -d --name bitwarden \
        -e ADMIN_TOKEN=${TOKEN} \
        -e LOG_FILE=/data/vaultwarden.log \
        -e LOG_LEVEL=warn -e EXTENDED_LOGGING=true \
        -e SMTP_HOST=in-v3.mailjet.com \
        -e SMTP_FROM=vaultwarden@sinu.xyz \
        -e SMTP_PORT=587 \
        -e SMTP_SECURITY="starttls" \
        -e SMTP_USERNAME=${SMTP_API_KEY} \
        -e SMTP_PASSWORD=${SMPT_SECRET} \
        --restart=always \
        -v /home/sinu/bw-data/:/data/ \
        -p 8080:80 \
        -p 3012:3012 \
        vaultwarden/server:latest
}

add_ssl_cert() {
    mkdir -p /var/www/html/test
    sudo certbot certonly --webroot -w /var/www/html/test -d sinujohn.xyz
}

add_nginx() {
    print_command "Installing nginx"
    sudo apt install -y nginx php7.4-fpm php7.4-cgi php7.4-xml php7.4-sqlite3 php7.4-intl apache2-utils
    sudo systemctl disable lighttpd
    sudo systemctl enable php7.4-fpm
    sudo systemctl enable nginx
    sudo rm /etc/nginx/sites-enabled/default
    sudo cp nginx.bitwarden.conf /etc/nginx/sites-enabled/bitwarden.conf
    sudo chown -R www-data:www-data /var/www/html
    sudo chmod -R 755 /var/www/html
    sudo usermod -aG pihole www-data
    sudo service php7.4-fpm start
    sudo systemctl restart nginx
    sudo ufw allow 'Nginx HTTPS'
}

setup_pi() {
    print_command "setting up pi"
    # set_raspi_config
    # add_utilities
    # setup_static_ip ;; Needs to be fixed
    # setup_ntp
    # add_security_features
    # setup_monitoring
    # setup_pihole
    # add_docker
    # add_bitwarden
    # add_ssl_cert
    add_nginx
}

setup_pi
