local M = {}

M.Setup = function()
    -- Install package manager
    --    https://github.com/folke/lazy.nvim
    --    `:help lazy.nvim.txt` for more info
    local lazypath = vim.fn.stdpath 'data' .. '/lazy/lazy.nvim'
    if not vim.loop.fs_stat(lazypath) then
        vim.fn.system {
            'git',
            'clone',
            '--filter=blob:none',
            'https://github.com/folke/lazy.nvim.git',
            '--branch=stable', -- latest stable release
            lazypath,
        }
    end
    vim.opt.rtp:prepend(lazypath)

    -- NOTE: Here is where you install your plugins.
    --  You can configure plugins using the `config` key.
    --
    --  You can also configure plugins after the setup call,
    --    as they will be available in your neovim runtime.
    require('lazy').setup({
        -- NOTE: First, some plugins that don't require any configuration

        -- Git related plugins
        'tpope/vim-fugitive',
        'tpope/vim-rhubarb',

        -- Detect tabstop and shiftwidth automatically
        'tpope/vim-sleuth',

        -- File Explorer
        {
            'preservim/nerdtree',
            config = function()
                --Exit Vim if NERDTree is the only window remaining in the only tab.
                vim.cmd [[autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif]]
            end
        },
        {
            'PhilRunninger/nerdtree-visual-selection',
            dependencies = {
                'preservim/nerdtree'
            },
        },

        -- NOTE: This is where your plugins related to LSP can be installed.
        --  The configuration is done below. Search for lspconfig to find it below.
        {
            -- LSP Configuration & Plugins
            'neovim/nvim-lspconfig',
            dependencies = {
                -- Automatically install LSPs to stdpath for neovim
                'williamboman/mason.nvim',
                'williamboman/mason-lspconfig.nvim',

                -- Useful status updates for LSP
                -- NOTE: `opts = {}` is the same as calling `require('fidget').setup({})`
                { 'j-hui/fidget.nvim', opts = {} },

                -- Additional lua configuration, makes nvim stuff amazing!
                'folke/neodev.nvim',
            },
        },

        {
            -- Autocompletion
            'hrsh7th/nvim-cmp',
            dependencies = { 'hrsh7th/cmp-nvim-lsp', 'L3MON4D3/LuaSnip', 'saadparwaiz1/cmp_luasnip' },
        },

        -- Useful plugin to show you pending keybinds.
        {
            'folke/which-key.nvim',
            dependencies = {
                'nvim-tree/nvim-web-devicons',
            },
            event = "VeryLazy",
            opts = {
                triggers = {
                    { "<auto>", mode = "nixsotc" },
                    -- { "<leader>", mode = { "n", "v" } },
                },
            },
            keys = { {
                "<leader>?",
                function()
                    require("which-key").show({ global = false })
                end,
                desc = "Buffer Local Keymaps (which-key)",
            },
            },
        },

        {
            -- Adds git releated signs to the gutter, as well as utilities for managing changes
            'lewis6991/gitsigns.nvim',
            opts = {
                -- See `:help gitsigns.txt`
                signs = {
                    add = { text = '+' },
                    change = { text = '~' },
                    delete = { text = '_' },
                    topdelete = { text = '‾' },
                    changedelete = { text = '~' },
                },
            },
        },

        {
            "zbirenbaum/copilot.lua",
            cmd = "Copilot",
            event = "InsertEnter",
            config = function()
                require("copilot").setup({
                    panel = {
                        enabled = true,
                        auto_refresh = true,
                    },
                    suggestion = {
                        enabled = true,
                        -- use the built-in keymapping for "accept" (<M-l>)
                        auto_trigger = true,
                        -- accept = false, -- disable built-in keymapping
                        keymap = {
                            accept = "<M-l>",
                            accept_word = false,
                            accept_line = false,
                            next = "<M-]>",
                            prev = "<M-[>",
                            dismiss = "<C-]>",
                        },
                    },
                })
            end,
        },

        {
            "CopilotC-Nvim/CopilotChat.nvim",
            branch = "canary",
            dependencies = {
                { "zbirenbaum/copilot.lua" }, -- or github/copilot.vim
                { "nvim-lua/plenary.nvim" }, -- for curl, log wrapper
            },
            build = "make tiktoken", -- Only on MacOS or Linux
            opts = {
                debug = true,       -- Enable debugging
                -- See Configuration section for rest
            },
            -- See Commands section for default commands if you want to lazy load on them
        },

        --Themes
        {
            'projekt0n/github-nvim-theme',
            lazy = false,
            priority = 1000,
            config = function()
                require('github-theme').setup({
                })
                vim.cmd('colorscheme github_dark')
            end,
        },

        {
            -- Set lualine as statusline
            'nvim-lualine/lualine.nvim',
            -- See `:help lualine.txt`
            opts = {
                sections = { lualine_c = { { 'filename', path = 1 } } },
                options = {
                    icons_enabled = false,
                    theme = 'onedark',
                    component_separators = '|',
                    section_separators = '',
                },
            },
        },

        'kyazdani42/nvim-web-devicons',

        {
            -- Add indentation guides even on blank lines
            'lukas-reineke/indent-blankline.nvim',
            -- Enable `lukas-reineke/indent-blankline.nvim`
            -- See `:help indent_blankline.txt`
            main = "ibl",
            opts = {
            },
        },

        -- "gc" to comment visual regions/lines
        { 'numToStr/Comment.nvim',         opts = {} },

        -- Fuzzy Finder (files, lsp, etc)
        { 'nvim-telescope/telescope.nvim', version = '*', dependencies = { 'nvim-lua/plenary.nvim' } },

        -- Fuzzy Finder Algorithm which requires local dependencies to be built.
        -- Only load if `make` is available. Make sure you have the system
        -- requirements installed.
        {
            'nvim-telescope/telescope-fzf-native.nvim',
            -- NOTE: If you are having trouble with this installation,
            --       refer to the README for telescope-fzf-native for more instructions.
            build = 'make',
            cond = function()
                return vim.fn.executable 'make' == 1
            end,
        },

        {
            'nvim-treesitter/nvim-treesitter',
            config = function()
                pcall(require('nvim-treesitter.install').update { with_sync = true })
            end
        },

        {
            -- Highlight, edit, and navigate code
            'nvim-treesitter/nvim-treesitter-textobjects',
            dependencies = {
                'nvim-treesitter/nvim-treesitter',
            },
        },

        { -- Add add bracket pairs
            'Raimondi/delimitMate'
        },

        {
            "ray-x/go.nvim",
            dependencies = { -- optional packages
                "ray-x/guihua.lua",
                "neovim/nvim-lspconfig",
                "nvim-treesitter/nvim-treesitter",
            },
            config = function()
                require("go").setup({
                    goimports = "golines",
                    gofmt = "golines",
                    max_line_len = 1000,
                })
            end,
            event = { "CmdlineEnter" },
            ft = { "go", 'gomod' },
            build = ':lua require("go.install").update_all_sync()' -- if you need to install/update all binaries
        },

        -- clojure
        -- eval clojure
        'Olical/conjure',
        -- https://github.com/Invertisment/conjure-clj-additions-cider-nrepl-mw

        -- equivalent of cider-jack-in
        {
            'clojure-vim/vim-jack-in',
            dependencies = {
                'tpope/vim-dispatch',
                'radenling/vim-dispatch-neovim'
            }
        },

        -- Clojure syntax highlighting
        { 'clojure-vim/clojure.vim', ft = { 'clojure', 'fennel' } },

        -- Sexp editing
        {
            'tpope/vim-sexp-mappings-for-regular-people',
            dependencies = {
                'guns/vim-sexp',
                'tpope/vim-repeat',
                'tpope/vim-surround'
            }
        },

        -- java
        'mfussenegger/nvim-jdtls',

        -- Smooth scrolling
        {
            'karb94/neoscroll.nvim',
            config = function()
                require('neoscroll').setup()
            end
        },

        --Markdown
        {
            'preservim/vim-markdown'
        },
        {
            "iamcco/markdown-preview.nvim",
            build = function() vim.fn["mkdp#util#install"]() end,
        },
        { 'godlygeek/tabular' },
        {
            'kevinhwang91/nvim-ufo',
            dependencies = {
                'kevinhwang91/promise-async'
            },
            config = function()
                vim.o.foldcolumn = '1' -- '0' is not bad
                vim.o.foldlevel = 99   -- Using ufo provider need a large value, feel free to decrease the value
                vim.o.foldlevelstart = 99
                vim.o.foldenable = true

                -- Using ufo provider need remap `zR` and `zM`. If Neovim is 0.6.1, remap yourself
                vim.keymap.set('n', 'zR', require('ufo').openAllFolds)
                vim.keymap.set('n', 'zM', require('ufo').closeAllFolds)
            end
        },

        {
            -- test runner support
            "vim-test/vim-test",
            config=function()
                vim.g["test#go#ginkgo#executable"] = "set -a; source test.env; ginkgo"
            end
        },

        {
            -- Highlight vars on cursor
            "RRethy/vim-illuminate"
        },

        {
            --Split single to multiline statements and vice-verca
            'AndrewRadev/splitjoin.vim'
        },
        { --protobuf support
            'uarun/vim-protobuf'
        },

        { -- outline symbols in a file
            'simrat39/symbols-outline.nvim', opts = {}
        },



        -- NOTE: Next Step on Your Neovim Journey: Add/Configure additional "plugins" for kickstart
        --       These are some example plugins that I've included in the kickstart repository.
        --       Uncomment any of the lines below to enable them.
        -- require 'kickstart.plugins.autoformat',
        require 'plugins.debug',

        -- NOTE: The import below automatically adds your own plugins, configuration, etc from `lua/custom/plugins/*.lua`
        --    You can use this folder to prevent any conflicts with this init.lua if you're interested in keeping
        --    up-to-date with whatever is in the kickstart repo.
        --
        --    For additional information see: https://github.com/folke/lazy.nvim#-structuring-your-plugins
        --
        --    An additional note is that if you only copied in the `init.lua`, you can just comment this line
        --    to get rid of the warning telling you that there are not plugins in `lua/custom/plugins/`.
        -- { import = 'custom.plugins' },
    }, {})
end

return M
