-- vim.filetype plugin indent on   -- allows auto-indenting depending on file type
-- Set Leader to <Space>
vim.g.mapleader = ' '
vim.g.maplocalleader = ','
vim.opt.autoindent = true                     --indent a new line the same amount as the line just typed
vim.opt.autoread = true                       -- Automatically reread changed files without asking me anything
vim.opt.completeopt = 'menu,menuone,noselect' -- Show Popup for completions
vim.opt.hlsearch = true                       -- highlight search results
vim.opt.ignorecase = true                     -- case insensitive matching
vim.opt.smartcase = true                      -- ... but not if it begins with upper case
vim.opt.incsearch = true                      -- Shows the match while typing
vim.opt.mouse = 'a'                           -- middle-click paste with mouse
vim.opt.backup = false                        -- Don't create annoying backup files
--vim.opt.nocompatible =true           -- disable compatibility to old-time vi
vim.opt.errorbells = false                    -- No beeps
vim.opt.swapfile = false                      -- Don't use swapfile
vim.opt.tabstop = 4                           -- number of columns occupied by a tab character
vim.opt.softtabstop = 4                       -- see multiple spaces as tabstops so <BS> does the right thing
vim.opt.shiftwidth = 4                        -- width for autoindents
vim.opt.expandtab = true                      -- converts tabs to white space
vim.opt.number = true                         -- add line numbers
vim.opt.wildmode = 'longest,list'             -- get bash-like tab completions
vim.opt.cc = '120'                            -- set an 120 column border for good coding style
vim.opt.syntax = 'ON'                         -- syntax highlighting
--vim.opt.t_Co=256                -- 256 colors
vim.opt.showmode = false                      -- hide vim mode from the bottom
vim.opt.cursorline = true                     -- highlight line on which cursor is placed
vim.opt.showcmd = false                       -- show leader key when typed
vim.opt.showmatch = true                      -- show matching brackets.
vim.opt.splitright = true                     -- new vertical splits are on the right
-- vim.opt.timeoutlen = 200                      -- time to show the which-key help
vim.opt.conceallevel = 2                      -- Hide syntax to show actual representation especially in markdown
-- vim.opt.scrolloff = 999                       -- Keep cursor at the center of the buffer
--vim.opt.iskeyword='+=-'         -- Treat dash separated words as a word text object
--vim.opt.formatoptions='-=cro'   -- Stop newline continuation of comments
vim.opt.spell = true
vim.opt.spelllang = "en_us"

--http://stackoverflow.com/questions/20186975/vim-mac-how-to-copy-to-clipboard-without-pbcopy
vim.o.clipboard = 'unnamed'
vim.o.clipboard = 'unnamedplus'

--Show relative numbers when buffer is focused, else absolute numbers
vim.cmd([[
augroup numbertoggle
autocmd!
autocmd BufEnter,FocusGained,InsertLeave,WinEnter * if &nu && mode() != "i" | set rnu   | endif
autocmd BufLeave,FocusLost,InsertEnter,WinLeave   * if &nu                  | set nornu | endif
augroup END
]])

--put quickfix window always to the bottom
vim.cmd([[
augroup quickfix
autocmd!
autocmd FileType qf wincmd J
autocmd FileType qf setlocal wrap
augroup END
]])

-- delimitMate
vim.g.delimitMate_expand_cr = 1
vim.g.delimitMate_expand_space = 1
vim.g.delimitMate_smart_quotes = 1
vim.g.delimitMate_expand_inside_quotes = 0
vim.g.delimitMate_smart_matchpairs = [[^\%(\w\|\$\)]]
vim.g.delimitMate_quotes = "\" '"

-- NERDTree
vim.g.NERDTreeShowHidden = 1
vim.g.NERDTreeIgnore={ 'node_modules' }

-- vim-test
vim.g['test#strategy'] = {
  nearest = 'dispatch',
  file = 'dispatch',
  suite = 'dispatch',
}

-- markdown
vim.cmd([[au FileType markdown setlocal foldlevel=99]])

-- Save on focus lost
vim.cmd([[au FocusLost * :wa]])

-- conjure
vim.g['conjure#mapping#doc_word'] = "K"
vim.g['conjure#client#clojure#nrepl#eval#auto_require'] = 0
vim.g['conjure#client#clojure#nrepl#connection#auto_repl#enabled'] = 0

-- vim-sexp
vim.g.sexp_filetypes = "clojure,scheme,lisp,timl,fennel,janet"


-- UltiSnips
-- snippet trigger
vim.g['UltiSnipsExpandTrigger'] = "<tab>"
-- list all snippets for current filetype
vim.g['UltiSnipsListSnippets'] = "<c-l>"
vim.g['UltiSnipsJumpForwardTrigger'] = "<tab>"
vim.g['UltiSnipsJumpBackwardTrigger'] = "<s-tab>"
vim.g['UltiSnipsSnippetDirectories'] = { "UltiSnips", "my_snippets" }

-- Open diagnostic when cursor is on error
function PrintDiagnostics(opts, bufnr, line_nr, client_id)
  bufnr = bufnr or 0
  line_nr = line_nr or (vim.api.nvim_win_get_cursor(0)[1] - 1)
  opts = opts or { ['lnum'] = line_nr }

  local line_diagnostics = vim.diagnostic.get(bufnr, opts)
  if vim.tbl_isempty(line_diagnostics) then return end

  local diagnostic_message = ""
  for i, diagnostic in ipairs(line_diagnostics) do
    diagnostic_message = diagnostic_message .. string.format("%d: %s", i, diagnostic.message or "")
    print(diagnostic_message)
    if i ~= #line_diagnostics then
      diagnostic_message = diagnostic_message .. "\n"
    end
  end
  vim.api.nvim_echo({ { diagnostic_message, "Normal" } }, false, {})
end

vim.o.updatetime = 250
vim.cmd [[autocmd CursorHold,CursorHoldI * lua vim.diagnostic.open_float(nil, {focus=false})]]

-- See `:help vim.o`
-- Trying out
-- Enable break indent
vim.o.breakindent = true

-- Save undo history
vim.o.undofile = true

-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'


-- Run gofmt + goimport on save
local format_sync_grp = vim.api.nvim_create_augroup("FormatOnSave", {})
vim.api.nvim_create_autocmd("BufWritePre", {
  pattern = "*.go",
  callback = function()
    require('go.format').goimport()
  end,
  group = format_sync_grp,
})

vim.api.nvim_create_autocmd("BufWritePre", {
  pattern = "*.{js,ts,jsx,tsx}",
  callback = function()
    vim.cmd(':OrganizeImports')
  end,
  group = format_sync_grp,
})

-- auto format when saving clojure files
vim.api.nvim_create_autocmd("BufWritePre", {
  pattern = "*.clj",
  callback = function()
    vim.lsp.buf.format({ async = true })
  end,
  group = format_sync_grp,
})
