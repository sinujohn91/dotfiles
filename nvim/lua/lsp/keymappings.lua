Keymappings = {}
Keymappings.Setup = function(bufnr)
    -- Mappings.
    -- See `:help vim.lsp.*` for documentation on any of the below functions

    local wk = require("which-key")
    wk.add({
        {"g", group = "GoTo"},
        {"gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", desc="Declaration" },
        {"gd", "<cmd>lua vim.lsp.buf.definition()<CR>", desc="Definition" },
        {"gi", "<cmd>lua vim.lsp.buf.implementation()<CR>", desc="Implementation" },
        {"gr", "<cmd>lua vim.lsp.buf.references()<CR>", desc="References" },
        {"gt", "<cmd>lua vim.lsp.buf.type_definition()<CR>", desc="Type Definition" },
    })

    wk.add({
        {"[d", "<cmd>lua vim.diagnostic.goto_prev()<CR>", desc="Previous Diagnostic" },
        {"]d", "<cmd>lua vim.diagnostic.goto_next()<CR>", desc="Next Diagnostic" },
    })

    wk.add({
        {"<leader>K", "<Cmd>lua vim.lsp.buf.hover()<CR>", desc="Show Information" },
        {
            {"<leader>h", group = "Help"},
            {"<leader>hs", "<cmd>lua vim.lsp.buf.signature_help()<CR>", desc="Signature" }
        },
        {
            {"<leader>r", group = "Refactor"},
            { "<leader>rr", "<cmd>lua vim.lsp.buf.rename()<CR>", desc="Rename Symbol" },
            {
                {"<leader>rf", group = "Format"},
                {"<leader>rfb", "<cmd>lua vim.lsp.buf.format({async = true})<CR>",desc="Format Buffer" },
            },
        },
        {
            {"<leader>c", group = "Code"},
            {"<leader>ca", "<cmd>lua vim.lsp.buf.code_action()<CR>", desc="Actions" }
        }
    })

    -- vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lh", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
    -- vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>le", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
    -- vim.api.nvim_buf_set_keymap(bufnr, "n", "<leader>lq", "<cmd>lua vim.diagnostic.setloclist()<CR>", opts)
end

return Keymappings
