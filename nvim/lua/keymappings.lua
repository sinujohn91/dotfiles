local km = vim.api.nvim_set_keymap

-- Remove highlight
km('n', '<Leader>h', ':set hlsearch!<CR>', { noremap = true, silent = true })

-- Better window navigation
km('n', '<C-h>', '<C-w>h', { silent = true })
km('n', '<C-j>', '<C-w>j', { silent = true })
km('n', '<C-k>', '<C-w>k', { silent = true })
km('n', '<C-l>', '<C-w>l', { silent = true })

-- Better indenting
km('v', '<', '<gv', { noremap = true, silent = true })
km('v', '>', '>gv', { noremap = true, silent = true })

-- Move selected line / block of text in visual mode
km('x', 'K', [[:move '<-2<CR>gv-gv']], { noremap = true, silent = true })
km('x', 'J', [[:move '>+1<CR>gv-gv']], { noremap = true, silent = true })

local wk = require("which-key")


km('n', '<Leader>/', '<cmd>Telescope live_grep<CR>', { silent = true, noremap = true })
-- Filetype specific key bindings

vim.cmd('autocmd FileType * lua setKeybinds()')
function setKeybinds()
    local fileTy = vim.api.nvim_buf_get_option(0, "filetype")
    local bufnr = vim.api.nvim_get_current_buf()

    -- File Explorer
    wk.add({
        { "<leader>f",  group = "File",                              remap = false },
        { "<leader>fb", desc = "<cmd>Telescope buffers<CR>",         remap = false },
        { "<leader>ff", "<cmd>Telescope find_files hidden=true<CR>", desc = "Fuzzy find files",         remap = false },
        { "<leader>fh", desc = "<cmd>Telescope help_tags<CR>",       remap = false },
        { "<leader>fs", ":NERDTreeFind<CR>",                         desc = "Open file in sidebar",     remap = false },
        { "<leader>ft", ":NERDTreeToggle<CR>",                       desc = "Toggle file tree sidebar", remap = false },
    })

    -- symbol outline
    wk.add({
        { "<leader>o", group = "Outline" },
        { "<leader>o", "<cmd>SymbolsOutline<CR>", desc = "Open outline" },
    })

    if fileTy == 'markdown' then
        wk.add({
            { "<leader>p", "<cmd>MarkdownPreviewToggle<CR>", desc = "Markdown Preview" },
            { "<CR>",      "<Plug>Markdown_Fold",            desc = "Fold" },
            { "==",        "<Plug>TableFormat",              desc = "Format Table" },
        })
    elseif fileTy == 'go' then
        wk.add({
            { "<leader>u",  group = "Utils" },
            { "<leader>ua", "<cmd>GoAddTag<CR>",                        desc = "Add tags to struct" },
            { "<leader>ur", "<cmd>GoRMTag<CR>",                         desc = "Remove tags to struct" },
            { "<leader>uc", "<cmd>GoCoverage<CR>",                      desc = "Test coverage" },
            { "<leader>ug", "<cmd>lua require('go.comment').gen()<CR>", desc = "Generate comment" },
            { "<leader>uv", "<cmd>GoVet<CR>",                           desc = "Go vet" },
            { "<leader>ut", "<cmd>GoModTidy<CR>",                       desc = "Go mod tidy" },
            { "<leader>ui", "<cmd>GoModInit<CR>",                       desc = "Go mod init" },
            {
                { "<leader>uf",  group = "Fill" },
                { "<leader>ufs", "<cmd>GoFillStruct<CR>", desc = "Struct" },
            },
        })
    end
end

--Easy vertical splitting
km('n', '<C-v>', ':wincmd v<CR>', { silent = true })

--Some useful quickfix shortcuts for quickfix
km('n', '<C-n>', ':cn<CR>', { silent = true })
km('n', '<C-p>', ':cp<CR>', { silent = true })
km('n', 'q', ':cclose<CR> <bar> :lclose<CR>', { silent = true })

-- Automatically resize screens to be equally the same
vim.cmd('autocmd VimResized * wincmd =')

-- Fast saving
km('n', '<Leader>w', ':w!<CR>', { silent = true, noremap = true })
km('n', '<Leader>q', ':q!<CR>', { silent = true, noremap = true })

-- vim-test
wk.add({
    { "<leader>t",  group = "Test" },
    { "<leader>tt", "<cmd>TestNearest<CR>", desc = "Run nearest test" },
    { "<leader>tf", "<cmd>TestFile<CR>",    desc = "Run test in current file" },
    { "<leader>ts", "<cmd>TestSuite<CR>",   desc = "Run current test suite" },
    { "<leader>tl", "<cmd>TestLast<CR>",    desc = "Run last run test" },
    { "<leader>tg", "<cmd>TestVisit<CR>",   desc = "Visit test file" },
})
vim.cmd('au FileType go nmap <silent> <leader>ta :Dispatch ginkgo ./...<CR>')

wk.add({
    { "<C-j>", function() require("copilot.suggestion").accept() end,  desc = "Accept Copilot Suggestion" },
    { "<C-k>", function() require("copilot.suggestion").next() end,    desc = "Next Copilot Suggestion" },
    { "<C-l>", function() require("copilot.suggestion").prev() end,    desc = "Previous Copilot Suggestion" },
    { "<C-]>", function() require("copilot.suggestion").dismiss() end, desc = "Dismiss Copilot Suggestion" },
    mode = "i",
})

-- CopilotC-Nvim/CopilotChat.nvim keybindings
-- :CopilotChatExplain - Write an explanation for the active selection as paragraphs of text
-- :CopilotChatReview - Review the selected code
-- :CopilotChatFix - There is a problem in this code. Rewrite the code to show it with the bug fixed
-- :CopilotChatOptimize - Optimize the selected code to improve performance and readability
-- :CopilotChatDocs - Please add documentation comment for the selection
-- :CopilotChatTests - Please generate tests for my code
-- :CopilotChatFixDiagnostic - Please assist with the following diagnostic issue in file
-- :CopilotChatCommit - Write commit message for the change with commitizen convention
-- :CopilotChatCommitStaged - Write commit message for the change with commitizen convention
wk.add({
    { "<leader>cc",  group = "CopilotChat" },
    { "<leader>ccc", function() require("CopilotChat").toggle() end, desc = "Toggle Copilot Chat" },
    {
        { "<leader>ccx",  "<CMD>CopilotChatExplain<CR>",       desc = "Explain the selected code" },
        { "<leader>ccr",  "<CMD>CopilotChatReview<CR>",        desc = "Review the selected code" },
        { "<leader>ccf",  "<CMD>CopilotChatFix<CR>",           desc = "Fix the selected code" },
        { "<leader>cco",  "<CMD>CopilotChatOptimize<CR>",      desc = "Optimize the selected code" },
        { "<leader>ccd",  "<CMD>CopilotChatDocs<CR>",          desc = "Add documentation for the selected code" },
        { "<leader>cct",  "<CMD>CopilotChatTests<CR>",         desc = "Generate tests for the selected code" },
        { "<leader>ccdi", "<CMD>CopilotChatFixDiagnostic<CR>", desc = "Fix the diagnostic issue in the selected code" },
        mode = "v",
    },
})
