# Zinit plugin manager
ZINIT_HOME="${XDG_DATA_HOME:-${HOME}/.local/share}/zinit/zinit.git"
[ ! -d $ZINIT_HOME ] && mkdir -p "$(dirname $ZINIT_HOME)"
[ ! -d $ZINIT_HOME/.git ] && git clone https://github.com/zdharma-continuum/zinit.git "$ZINIT_HOME"
source "${ZINIT_HOME}/zinit.zsh"

# Load plugins
## git clone depth
zinit ice depth"1"

## Terminal prompt
# Load starship theme
# line 1: `starship` binary as command, from github release
# line 2: starship setup at clone(create init.zsh, completion)
# line 3: pull behavior same as clone, source init.zsh
zinit ice as"command" from"gh-r" \
          atclone"./starship init zsh > init.zsh; ./starship completions zsh > _starship" \
          atpull"%atclone" src"init.zsh"
zinit light starship/starship

# zinit light zsh-users/zsh-history-substring-search

## Syntax highlighting, completions, and autosuggestions
zinit light zsh-users/zsh-syntax-highlighting
zinit light zsh-users/zsh-completions
zinit light zsh-users/zsh-autosuggestions
zinit light Aloxaf/fzf-tab

## oh-my-zsh git alias
zinit snippet OMZP::git

# Load completions
autoload -Uz compinit && compinit

zinit cdreplay -q

# Load vim key bindings
bindkey -v

# History
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt extendedhistory
setopt hist_expire_dups_first
setopt hist_ignore_dups
setopt hist_ignore_space
setopt hist_find_no_dups
setopt hist_verify
setopt inc_append_history
setopt share_history

# Completion settings
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Z}'
zstyle ':completion:*' list-colors '${(s.:.)LS_COLORS}'
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'

# Aliases
alias ls='ls --color=auto'

# Shell integrations
eval "$(fzf --zsh)"
eval "$(zoxide init zsh)"


# Git Settings
git config --global branch.sort -committerdate
git config --global core.editor nvim
git config --global pull.rebase true
git config --global push.default current

# Path
export PATH=/Users/sinu/bin:$PATH
export PATH=$PATH:.:/usr/local/sbin:/Users/sinu/Library/Python/2.7/bin
export PATH="$HOME/tools/lua-language-server/bin/macOS:$PATH"

export GOPATH=$(go env GOPATH)
export PATH=$PATH:$GOPATH/bin
export GOPRIVATE="source.golabs.io"
export GOPROXY="https://proxy.golang.org,direct"

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
  export EDITOR='vim'
else
  export EDITOR='nvim'
fi

# GoCloud SDK
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/sinu/Downloads/google-cloud-sdk 2/path.zsh.inc' ]; then . '/Users/sinu/Downloads/google-cloud-sdk 2/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/sinu/Downloads/google-cloud-sdk 2/completion.zsh.inc' ]; then . '/Users/sinu/Downloads/google-cloud-sdk 2/completion.zsh.inc'; fi
eval "$(gh copilot alias -- zsh)"

# NVM
export NVM_DIR="$HOME/.nvm"
[ -s "/usr/local/opt/nvm/nvm.sh" ] && \. "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && \. "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion
