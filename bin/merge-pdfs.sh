#! /bin/sh -e

outputFilename=$1
inputDirectory=$2

files=$(ls -1 $inputDirectory | sort -V)

/System/Library/Automator/Combine\ PDF\ Pages.action/Contents/Resources/join.py -o ${outputFilename} $files

