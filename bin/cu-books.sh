#!/bin/sh -ex

URL_PREFIX=$1
START_PAGE=$2
END_PAGE=$3
BOOK_NAME=$4

download_image() {
    url="$1"
    filename="$2"
    echo "downloading $1 $2"
    curl "$url" --output "$filename" --silent
}

download_images() {
    url_prefix=$1
    start_page=$2
    end_page=$3
    mkdir "${BOOK_NAME}"
    for i in $(seq ${start_page} ${end_page})
    do
        download_image "${url_prefix}/${i}.jpg" "./$BOOK_NAME/${i}.jpg"
    done
}

create_book() {
    echo "Downloading book $URL_PREFIX $START_PAGE $END_PAGEA"
    download_images "$URL_PREFIX" "$START_PAGE" "$END_PAGE"
    rm -rf "${BOOK_NAME}".pdf
    magick convert $(ls -1 ./"${BOOK_NAME}"/*.jpg | sort -V) -quality 100 "${BOOK_NAME}.pdf"
}

create_book


# ./cu-books.sh https://online.pubhtml5.com/kcvf/cwsa/files/large 1 33 cwsa && ./cu-books.sh https://online.pubhtml5.com/kcvf/bhaf/files/large 1 20 bhaf && ./cu-books.sh https://online.pubhtml5.com/kcvf/gotj/files/large 1 28 gotj && ./cu-books.sh https://online.pubhtml5.com/kcvf/bdui/files/large 1 68 bdui && ./cu-books.sh https://online.pubhtml5.com/kcvf/ledg/files/large 1 20 ledg && ./cu-books.sh https://online.pubhtml5.com/kcvf/dnbd/files/large 1 25 dnbd && ./cu-books.sh https://online.pubhtml5.com/kcvf/xigo/files/large 1 16 xigo && ./cu-books.sh https://online.pubhtml5.com/kcvf/hdkh/files/large 1 18 hdkh && ./cu-books.sh https://online.pubhtml5.com/kcvf/zefp/files/large 1 20 zefp && ./cu-books.sh https://online.pubhtml5.com/kcvf/wmab/files/large 1 29 wmab




# ./cu-books.sh https://online.pubhtml5.com/kcvf/fjld/files/large 1 37 fjld && ./cu-books.sh https://online.pubhtml5.com/kcvf/owqs/files/large 1 32 owqs && ./cu-books.sh https://online.pubhtml5.com/kcvf/xnxx/files/large 1 30 xnxx && ./cu-books.sh https://online.pubhtml5.com/kcvf/oweu/files/large 1 20 oweu && ./cu-books.sh https://online.pubhtml5.com/kcvf/iqne/files/large 1 33 iqne && ./cu-books.sh https://online.pubhtml5.com/kcvf/aqva/files/large 1 33 aqva && ./cu-books.sh https://online.pubhtml5.com/kcvf/amxw/files/large 1 37 amxw && ./cu-books.sh https://online.pubhtml5.com/kcvf/frje/files/large 1 29 frje && ./cu-books.sh https://online.pubhtml5.com/kcvf/alvw/files/large 1 304 alvw && ./cu-books.sh https://online.pubhtml5.com/kcvf/egwb/files/large 1 316 egwb && ./cu-books.sh https://online.pubhtml5.com/kcvf/cxxq/files/large 1 30 cxxq
