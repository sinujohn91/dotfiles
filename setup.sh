#!/bin/bash

ln -sf ~/dotfiles/.zshrc ~/.zshrc
ln -sf ~/dotfiles/.spacemacs ~/.spacemacs
ln -sf ~/dotfiles/.vimrc ~/.vimrc
ln -sf ~/dotfiles/nvim ~/.config/nvim
ln -sf ~/dotfiles/clojure-lsp ~/.config/clojure-lsp
ln -sf ~/dotfiles/clj-kondo ~/.config/clj-kondo
ln -sf ~/dotfiles/alacritty ~/.config/alacritty
ln -sf ~/dotfiles/.tmux.conf ~/.tmux.conf
ln -sf ~/dotfiles/.hammerspoon ~/.hammerspoon

# Setup terminal
## Terminal Font
brew tap homebrew/cask-fonts
brew install --cask font-jetbrains-mono

## Git
brew install gh
gh auth login
gh extension install github/gh-copilot

## Fuzzy Search
brew install fzf

## Better cd
brew install zoxide

# Install neovim
brew install neovim

# Language
brew install node
brew install go

# LSP servers for nvim
npm i -g vscode-langservers-extracted
brew remove clojure-lsp # if you have old clojure-lsp installed via brew
brew install clojure-lsp/brew/clojure-lsp-native
npm install -g typescript typescript-language-server
brew install jdtls
npm i -g pyright

brew install docker
brew install docker-compose
brew install colima   # docker runtime
brew install ripgrep
brew install tmux
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
brew install lua-language-server
brew install go-task

# Install language env
brew install luarocks #lua package manager

colima start --mount $HOME:/:w

Utility
brew install --cask hammerspoon
brew install --cask raycast
brew install --cask postico
brew install --cask sublime-text
brew install --cask mockoon

